# encoding: utf-8

from __future__ import absolute_import

import logging
from unittest import TestCase

from ldap import SCOPE_SUBTREE

import xldap
from xldap.ldapobject import XLDAPObject


class Test(TestCase):
    def setUp(self):
        logging.basicConfig(level=logging.DEBUG)
        self.context = 'dc=csusb,dc=edu'
        xldap.register_context(self.context, 'ldap://dc1.csusb.edu')

    def test_get(self):
        l = XLDAPObject.connect_naming_context(self.context)
        entry = l.get('cn=005398441,cn=users,dc=csusb,dc=edu')
        self.assertIsInstance(entry, dict)

    def test_find_one(self):
        l = XLDAPObject.connect_naming_context(self.context)
        dn, entry = l.find_one(filterstr='cn=005398441')
        self.assertIsNotNone(dn)
        self.assertIsInstance(entry, dict)

    def test_paged_search(self):
        l = XLDAPObject.connect_naming_context(self.context)
        cn_list = ['005398441', '000024400', '000030081']
        assertions = ['(cn={})'.format(cn) for cn in cn_list]
        filterstr = '(|{})'.format(''.join(assertions))
        result = [entry['cn'][0] for dn, entry
                  in l.paged_search(self.context, SCOPE_SUBTREE,
                                    page_size=1,
                                    filterstr=filterstr)
                  if dn]
        self.assertEqual(set(cn_list), set(result))

    def test_default_naming_context(self):
        l = XLDAPObject.connect_naming_context(self.context)
        dn = l.default_naming_context
        self.assertEqual(dn.lower(), self.context.lower())