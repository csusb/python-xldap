# encoding: utf-8

from __future__ import absolute_import

import logging
from unittest import TestCase

import ldap

import xldap
import xldap.auth


class TestAuthCallbacks(TestCase):
    def setUp(self):
        logging.basicConfig(level=logging.DEBUG)
        xldap.register_context('dc=csusb,dc=edu', 'ldap://dc1.csusb.edu')
        self.ldapobj = xldap.connect_naming_context('dc=csusb,dc=edu')

    def test_simple_bind_with_uid(self):
        callback = xldap.auth.simple_bind_with_uid('dc=csusb,dc=edu',
                                                   uid='005398441',
                                                   password='foobarbaz')
        self.assertRaises(ldap.INVALID_CREDENTIALS, callback, self.ldapobj)

    def test_auto_sasl_bind(self):
        self.assertRaises(ldap.LDAPError, xldap.auth.auto_sasl_bind, self.ldapobj)

    def tearDown(self):
        self.ldapobj.unbind_s()
