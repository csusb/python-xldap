# encoding: utf-8

from __future__ import absolute_import

from ldap import NO_SUCH_OBJECT

from xldap import connect_naming_context
from xldap.attribute import Attribute


class Entry(object):
    def __init__(self, ldapobj, dn):
        self.ldapobj = ldapobj
        self.dn = dn

    @classmethod
    def new(cls, dn):
        ldapobj = connect_naming_context(dn)
        return cls(ldapobj, dn)

    @property
    def exists(self):
        try:
            self.ldapobj.get(self.dn, attlist=[])
        except NO_SUCH_OBJECT:
            return False
        else:
            return True

    def items(self):
        return self.ldapobj.get(self.dn).items()

    def get(self, attrname, default=None):
        attribute = self.new_attribute(attrname)
        if attribute.exists:
            return attribute
        else:
            return default

    def keys(self):
        return self.ldapobj.get(self.dn, attrlist=[]).keys()

    def asdict(self):
        return dict(self.items())

    @property
    def attribute_types(self):
        return self.ldapobj.schema.attribute_types(self['objectClass'])

    def new_attribute(self, attrname):
        return Attribute(self, attrname)

    def __getitem__(self, attrname):
        attribute = self.get(attrname)
        if not attribute:
            raise KeyError('Attribute does not exist')
        else:
            return attribute

    def __contains__(self, attrname):
        return self.new_attribute(attrname).exists

    def __repr__(self):
        return '<{0.__class__.__name__} {0.dn}>'.format(self)

    def __str__(self):
        return self.dn
