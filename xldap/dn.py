# encoding: utf-8

from __future__ import absolute_import

from ldap.dn import dn2str, str2dn


def walk_dn(context):
    dn = str2dn(context)
    while True:
        if dn:
            yield dn2str(dn)
            dn.reverse()
            dn.pop()
            dn.reverse()
        else:
            break
