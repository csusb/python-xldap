# encoding: utf-8

from __future__ import absolute_import

import sys
import logging
from getpass import getuser, getpass

from ldap import LDAPError
from ldap import sasl
from ldap.filter import escape_filter_chars

logger = logging.getLogger(__name__)


class AutoSASL(sasl.sasl):
    SUPPORTED_MECHS = (
        (0, 'EXTERNAL'),
        (1, 'GSSAPI'),
        (2, 'DIGEST-MD5'),
        (3, 'CRAM-MD5'),
    )

    @classmethod
    def supported(cls, ldapobj):
        for priority, name in sorted(cls.SUPPORTED_MECHS):
            if name not in ldapobj.rootdse['supportedSASLMechanisms']:
                continue
            if name in ('EXTERNAL', 'GSSAPI'):
                yield cls({sasl.CB_USER: ''}, name)
            if name.endswith('MD5'):
                if sys.stdin.isatty():
                    yield cls({sasl.CB_AUTHNAME: getuser(), sasl.CB_PASS: getpass()}, name)

    def __str__(self):
        if self.mech.endswith('MD5'):
            return '{} ({})'.format(self.mech, self.cb_value_dict[sasl.CB_AUTHNAME])
        else:
            return self.mech


def auto_sasl_bind(ldapobj):
    for mech in AutoSASL.supported(ldapobj):
        ldapobj.logger.debug('Attempting SASL %s bind...', mech)
        try:
            ldapobj.sasl_interactive_bind_s('', mech)
        except LDAPError as error:
            ldapobj.logger.debug(error)
            continue
        else:
            ldapobj.logger.info('Bound via SASL %s', mech)
            ldapobj.logger.info('SASL Identity: %s', ldapobj.whoami_s())
            break
    else:
        raise LDAPError('Unable to bind via any supported SASL mechanism')


def simple_bind_with_uid(context, filter_template=None, uid=None, password=None):
    if not uid:
        uid = getuser()
    if not password:
        if sys.stdin.isatty():
            password = getpass()
        else:
            raise LDAPError('Password not provided and unable to prompt for one')

    def callback(ldapobj):
        if filter_template:
            _filter_template = filter_template
        else:
            if 'domainControllerFunctionality' in ldapobj.rootdse:
                _filter_template = 'sAMAccountName={}'  # microsoft
            else:
                _filter_template = 'uid={}'  # everybody else
        filterstr = _filter_template.format(escape_filter_chars(uid))
        ldapobj.logger.debug('Finding DN for bind under %s with filter: %s', context, filterstr)
        dn, _ = ldapobj.find_one(filterstr=filterstr)
        ldapobj.logger.debug('Performing bind with %s', dn)
        ldapobj.simple_bind_s(dn, password)
        ldapobj.logger.info('Bound as %s', ldapobj.whoami_s())

    return callback
