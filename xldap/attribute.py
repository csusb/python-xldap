# encoding: utf-8

from __future__ import absolute_import
from __future__ import print_function

import logging

from ldap import COMPARE_FALSE, COMPARE_TRUE
from ldap.schema import AttributeType


class Attribute(object):
    def __init__(self, entry, name):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.name = name
        self.entry = entry
        self.ldapobj = entry.ldapobj

    @property
    def schema(self):
        return self.ldapobj.schema.get_obj(AttributeType, self.name)

    @property
    def exists(self):
        return self.name in self.entry.keys()

    @property
    def values(self):
        entry = self.ldapobj.get(self.entry.dn, attrlist=[self.name.encode('utf-8')])
        return entry[self.name]

    def _compare(self, value):
        rid = self.ldapobj.compare(self.entry.dn, self.name, value)
        try:
            self.ldapobj.result(rid, timeout=3)
        except COMPARE_TRUE:
            return True
        except COMPARE_FALSE:
            return False
        except:
            self.ldapobj.cancel(rid)
            raise
        else:
            raise Exception('should not happen')

    def __get_slice__(self, i, j):
        return [self[x] for x in xrange(i, j)]

    def __getitem__(self, key):
        return self.values[key]

    def __repr__(self):
        return '<{0.__class__.__name__} {0.entry.dn}:{0.name}>'.format(self)

    def __eq__(self, value):
        return self._compare(value)
    __contains__ = __eq__

    def __len__(self):
        return len(self.values)
