# encoding: utf-8

from __future__ import absolute_import

from xldap.dn import walk_dn


def register_context(naming_context, host_uri, start_tls=False, options=None, auth_callback=None):
    global _context_map
    _context_map[naming_context.lower()] = (host_uri, start_tls, options, auth_callback)


def match_context(context):
    global _context_map
    for dn in walk_dn(context):
        try:
            return _context_map[dn.lower()]
        except KeyError:
            continue
    else:
        raise KeyError('Unable to match context')

_context_map = {}
