# encoding: utf-8

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import logging

from ldap import LDAPError, NO_UNIQUE_ENTRY
from ldap import OPT_X_TLS_NEWCTX
from ldap import SCOPE_BASE, SCOPE_SUBTREE
from ldap.controls import SimplePagedResultsControl
from ldap.ldapobject import LDAPObject
from ldap.schema import SubSchema

from xldap.context import match_context

logger = logging.getLogger(__name__)


class XLDAPObject(LDAPObject):
    @classmethod
    def connect_naming_context(cls, context, auth_callback=None):
        uri, start_tls, options, _auth_callback = match_context(context)
        obj = cls(uri)
        if options:
            for flags in options:
                obj.set_option(*flags)

        if start_tls:
            obj.start_tls_s()

        if auth_callback:
            auth_callback(obj)
        elif _auth_callback:
            _auth_callback(obj)
        else:
            obj.simple_bind_s('', '')

        return obj

    def __init__(self, *args, **kwargs):
        LDAPObject.__init__(self, *args, **kwargs)
        self.logger = logging.getLogger(self.__class__.__name__)

    def start_tls_s(self):
        self.set_option(OPT_X_TLS_NEWCTX, 0)
        LDAPObject.start_tls_s(self)

    def get(self, dn, timeout=3, **kwargs):
        """ entry = ldapobj.get(dn) """
        _, entry = self.search_st(dn, SCOPE_BASE, timeout=timeout, **kwargs)[0]
        return entry

    def find_one(self, base=None, scope=SCOPE_SUBTREE, **kwargs):
        if not base:
            base = self.default_naming_context
        result = [(dn, entry) for dn, entry in self.search_st(base, scope, **kwargs) if dn]
        if len(result) != 1:
            raise NO_UNIQUE_ENTRY(kwargs['filterstr'])
        else:
            return result[0]

    def paged_search(self, base, scope, page_size=1000, **kwargs):
        pctrl = SimplePagedResultsControl(criticality=True, size=page_size, cookie='')
        msgid = self.search_ext(base, scope, serverctrls=[pctrl], **kwargs)
        while True:
            rtype, rdata, rmsgid, rctrls = self.result3(msgid)
            for dn, entry in rdata:
                yield dn, entry

            pctrls = [ c for c in rctrls if c.controlType == pctrl.controlType ]
            try:
                cookie = pctrls[0].cookie
            except IndexError:
                raise LDAPError('Expected Page Control from server')

            if cookie:
                pctrl.cookie = cookie
                msgid = self.search_ext(base, scope, serverctrls=[pctrl], **kwargs)
            else:
                break

    @property
    def rootdse(self):
        _, entry = self.search_st('', SCOPE_BASE, timeout=3)[0]
        return entry

    @property
    def default_naming_context(self):
        if 'defaultNamingContext' in self.rootdse:
            return self.rootdse['defaultNamingContext'][0]
        else:
            return self.rootdse['namingContexts'][0]

    @property
    def schema(self):
        dn = self.rootdse['subschemaSubentry'][0]
        entry = self.get(dn, attrlist=[b'*', b'+', b'objectClasses', b'attributeTypes'])
        return SubSchema(entry)


connect_naming_context = XLDAPObject.connect_naming_context
