# encoding: utf-8

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

import shelve
import logging

from ldap import SCOPE_BASE, SCOPE_ONELEVEL
from ldap import RES_SEARCH_ENTRY
from ldap.controls import RequestControl

from xldap import connect_naming_context

logger = logging.getLogger(__name__)

MSDS_LDAP_SERVER_NOTIFICATION_OID = b'1.2.840.113556.1.4.528'

class USNTracker(object):
    def __init__(self, state_file_path, ldapobj):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.ldap = ldapobj
        self.db = shelve.open(state_file_path)

    @classmethod
    def new(cls, naming_context, state_file_path='usn'):
        l = connect_naming_context(naming_context)
        obj = cls(state_file_path, l)
        return obj

    def close(self):
        self.db.close()

    @property
    def invocation_id(self):
        ds_service_name = self.ldap.rootdse['dsServiceName'][0]
        dn, entry = self.ldap.search_st(ds_service_name, SCOPE_BASE, timeout=3)[0]
        return entry['invocationId'][0].encode('hex')

    @property
    def highest_committed_usn(self):
        return int(self.ldap.rootdse['highestCommittedUSN'][0])

    @property
    def usn(self):
        try:
            return self.db[self.invocation_id]['usn']
        except KeyError:
            return 0

    @usn.setter
    def usn(self, value):
        self.logger.debug('Highest USN: %s', value)
        state = self.db.get(self.invocation_id, {})
        state['usn'] = value
        self.db[self.invocation_id] = state
        self.db.sync()

    def listen(self, base):
        controls = [RequestControl(MSDS_LDAP_SERVER_NOTIFICATION_OID, criticality=True)]
        filterstr='objectClass=*'
        msgid = self.ldap.search_ext(base, SCOPE_ONELEVEL, filterstr, serverctrls=controls)
        while True:
            rtype, rdata, rmsgid, rcontrols = self.ldap.result3(msgid=msgid, all=0)
            if not rdata:
                break
            if rtype == RES_SEARCH_ENTRY:
                for dn, _ in rdata:
                    dn, entry = self.ldap.search_st(dn, SCOPE_BASE)[0]
                    usn = int(entry['uSNChanged'][0])
                    if usn <= self.usn:
                        continue
                    else:
                        yield dn, entry
                        self.usn = usn

    def __enter__(self):
        self.__usn_enter = self.highest_committed_usn

    def __exit__(self, exec_type, exc_value, traceback):
        if not exc_value:
            self.usn = self.__usn_enter
        return False
