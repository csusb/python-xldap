# encoding: utf-8

from __future__ import absolute_import

from xldap.ldapobject import connect_naming_context
from xldap.dn import walk_dn
from xldap.context import register_context

# API compatibility shims
from xldap.context import register_context as register_dit
from xldap.dn import walk_dn as walk_context


def connect_bound_naming_context(dn):
    from xldap.auth import auto_sasl_bind
    ldapobj = connect_naming_context(dn, auth_callback=auto_sasl_bind)
    return ldapobj
