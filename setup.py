from setuptools import setup, find_packages

setup(
    name='xldap',
    url='https://bitbucket.org/csusb/python-xldap',
    maintainer='ISET, California State University San Bernardino',
    maintainer_email='iset-ops@csusb.edu',
    version=open('VERSION').read().strip(),
    packages=find_packages(exclude=['tests']),
    install_requires = open('requirements.txt').readlines()
)
